import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

load = np.load('hw1.npy', allow_pickle=True)
depth = load[()]["depth"] #4998x1
faces = load[()]["faces"] #9999x3
vcolors = load[()]["vcolors"] #4998x3
verts2d = load[()]["verts2d"] #4998x2
height = 512
width = 512
img = 255*np.ones((height,width,3), np.uint8)

def shade_triangle(img,verts2d,vcolors,shade_t):
    Y = img
    for i in range(0,verts2d.shape[0]-1):
        Y[int(verts2d[i,1]),int(verts2d[i,0]),:] = (0,0,0)
    return Y


def interpolate_color():
    """
     Function that implements linear interpolation between 2 3D points
     Inputs:
        x1 : horizontal components of C1, C2
        x2 : vertical components of C1, C2
        x  : point to apply linear interpolation
        C1 : 3D color values corresponding to vertices of x1 and x2
    Outputs:
        value : value that is derived from linear interpolation of C1, C2
    """

    return 5

shade_t = "flat"

# Y = shade_triangle(img,verts2d,vcolors,shade_t)

# def new_akmes(x_y_min_max,akmes,y):
#     copy = []
#     copy = x_y_min_max.copy()
#     copy_akmes = akmes.copy()
#     for i in range(len(copy)-1):
#         if y == copy[i][3]:
#             x_y_min_max.remove(x_y_min_max[i][:])
#             akmes.remove(akmes[i])

#     for i in range(len(x_y_min_max)-1):
#         xmin = x_y_min_max[i][0]
#         ymin = x_y_min_max[i][1]
#         xmax = x_y_min_max[i][2]
#         ymax = x_y_min_max[i][3]
#         if (xmax-xmin) != 0:
#             mk = (ymax-ymin)/(xmax-xmin)
#             akmes[i][0] = akmes[i][0] + 1/mk
#         else:
#             akmes[i][0] = x_y_min_max[i][0]

#     return x_y_min_max , akmes

def new(x_y_min_max,y):

    new_matrix = []

    for i in range(len(x_y_min_max)-1):
        if x_y_min_max[i][3] > y:
            new_matrix.append(x_y_min_max[i])

    return new_matrix

def new_energa(energa,x_y_min_max):

    new_matrix = []

    for i in range(len(x_y_min_max)-1):
        xmin = x_y_min_max[i][0]
        ymin = x_y_min_max[i][1]
        xmax = x_y_min_max[i][2]
        ymax = x_y_min_max[i][3]
        if energa[i][1] < y:
            energa[i][1] = energa[i][1] + 1
            if xmax-xmin != 0:
                mk = (ymax-ymin)/(xmax-xmin)
                energa[i][0] = energa[i][0] + 1/mk
            else:
                energa[i][0] = energa[i][0]
        new_matrix.append(energa[i])

    return new_matrix

def takeFirst(elem):
    return elem[0]

x_y_min_max = []
energa = []

for y in range(0,height-1):

    # [x_y_min_max, akmes] = new_akmes(x_y_min_max,akmes,y)
    
    x_y_min_max = new(x_y_min_max,y)
    energa = new_energa(energa,x_y_min_max)

    energa.sort(key=takeFirst)


    for i in range(0,verts2d.shape[0]-1):
        if verts2d[i,1] == y:
            for j in range(0,faces.shape[0]-1):
                for k in range(0,faces.shape[1]-1):
                    if faces[j,k] == i:
                        for l in range(0,faces.shape[1]-1):
                            if l != k:
                                if verts2d[faces[j,l],1]>verts2d[faces[j,k],1]:
                                    x_y_min_max.append([verts2d[faces[j,k],0],verts2d[faces[j,k],1],verts2d[faces[j,l],0],verts2d[faces[j,l],1]])
                                    energa.append([verts2d[faces[j,k],0],verts2d[faces[j,k],1]])

    for i in range(0,len(x_y_min_max)-1):
        img[y,int(energa[i][0]),:] = (0,0,0)

cv.imshow('Binary',img)
cv.waitKey(0)
